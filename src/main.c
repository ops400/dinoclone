#include <raylib.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

const int widonwWidth = 900;
const int widonwHeight= 480;
const char* windowTitle = "Clone Dino";

const int offScreen = widonwWidth+1;

const int catusType1x = 12;
const int catusType1y = 46;
const int catusType2x = 19;
const int catusType2y = 65;
const int gravity = 3;
const int speed = 5;

int randomNumBuffer;
bool isPlayerDead = false;

bool isCactusOnScreen(int a);
int randomNumber();

struct obistacle{
    Vector2 pos, size;
    Color clr;
    Rectangle rec;
};

struct player{
    Vector2 pos, size;
    Color clr;
    Rectangle rec;
    unsigned int sizeDiferencial, jumpHeight, startJump, endJump, isDown;
    bool playerCrouching, isPlayerJumping;
};

int main(int argc, char* argv[]){
    struct obistacle cactus1, cactus2, cactus3, cactus4, cactus5, cactus6, bird1, bird2, ground;
    cactus1.size.x = cactus2.size.x = cactus3.size.x = 12;
    cactus1.size.y = cactus2.size.y = cactus3.size.y = 46;
    cactus4.size.x = cactus5.size.x = cactus6.size.x = 19;
    cactus4.size.y = cactus5.size.y = cactus6.size.y = 65;

    cactus1.pos.x = cactus2.pos.x = cactus3.pos.x = offScreen+catusType1x;
    cactus1.pos.y = cactus2.pos.y = cactus3.pos.y = 422-(cactus1.size.y-1);
    cactus4.pos.x = cactus5.rec.x = cactus6.rec.x = (float)widonwWidth/2;
    cactus4.pos.y = cactus5.pos.y = cactus6.pos.y = 422-(cactus4.size.y-1);

    cactus1.rec.width = cactus2.rec.width = cactus3.rec.width = catusType1x;
    cactus1.rec.height = cactus2.rec.height = cactus3.rec.height = catusType1y;
    cactus4.rec.width = cactus5.rec.width = cactus6.rec.width = catusType2x;
    cactus4.rec.height = cactus5.rec.height = cactus6.rec.height = catusType2y;

    cactus1.rec.x = cactus2.rec.x = cactus3.rec.x = cactus1.pos.x;
    cactus1.rec.y = cactus2.rec.y = cactus3.rec.y = cactus1.pos.y;
    cactus4.rec.x = cactus5.rec.x = cactus6.rec.x = cactus4.pos.x;
    cactus4.rec.y = cactus5.rec.y = cactus6.rec.y = cactus4.pos.y;

    cactus4.clr = cactus5.clr = cactus6.clr = BLUE;
    cactus1.clr = cactus2.clr = cactus3.clr = GREEN;

    struct player ply;
    ply.pos.x = 56;
    ply.pos.y = 362;
    ply.size.x = 66;
    ply.size.y = 61;
    ply.jumpHeight = 250;
    ply.rec.x = ply.pos.x;
    ply.rec.y = ply.pos.y;
    ply.rec.width = ply.size.x;
    ply.rec.height = ply.size.y;
    ply.isPlayerJumping = false;
    ply.startJump = 362;
    ply.endJump = 262;
    ply.isDown = 0;
    ply.clr = ORANGE;
    ply.playerCrouching = false;

    ground.rec.x = 0;
    ground.rec.y = 423;
    ground.rec.width = widonwWidth;
    ground.rec.height = 57;
    ground.clr = GRAY;

    ply.pos.x = 56;
    InitWindow(widonwWidth, widonwHeight, windowTitle);
    SetTargetFPS(60);
    while(!WindowShouldClose()){
        if(isPlayerDead == false){
            if(CheckCollisionRecs(ply.rec, cactus1.rec) ||
            CheckCollisionRecs(ply.rec, cactus4.rec)){
                isPlayerDead = true;
            }
            cactus1.pos.x = cactus1.pos.x - speed;
            cactus4.pos.x = cactus4.pos.x - speed;

            cactus1.rec.x = cactus1.pos.x;

            cactus2.rec.x = cactus1.pos.x + cactus1.size.x + 2;

            cactus3.rec.x = cactus2.rec.x + cactus1.size.x + 2;

            cactus4.rec.x = cactus4.pos.x;

            cactus5.rec.x = cactus4.rec.x + cactus4.size.x + 2;

            cactus6.rec.x = cactus5.rec.x + cactus5.size.x + 2;

            if(IsKeyDown(KEY_DOWN)) ply.playerCrouching = true;
            if(IsKeyReleased(KEY_DOWN)) ply.playerCrouching = false;
            if(ply.playerCrouching){
                ply.rec.height = 32;
            }
            else if(!ply.playerCrouching){
                ply.rec.height = 61;
            }
            if(!ply.playerCrouching &&
            ply.pos.y == 392){
                ply.pos.y = 362;
            }
            if(IsKeyPressed(KEY_SPACE) && CheckCollisionRecs(ply.rec, ground.rec)){
                ply.isPlayerJumping = true;
            }
            if(ply.isPlayerJumping == true){
                ply.pos.y -= gravity*((float)speed/5);
            }
            if(ply.pos.y <= ply.jumpHeight) ply.isPlayerJumping = false;
            if(ply.isPlayerJumping == false &&
            !CheckCollisionRecs(ply.rec, ground.rec)){
                ply.pos.y += gravity;
            }
            ply.rec.x = ply.pos.x;
            ply.rec.y = ply.pos.y;
            printf("FPS:%i\n", GetFPS());
            printf("cactus1.pos.x%f\n",ply.pos.y);
            printf("DebugBool%i\n", ply.isPlayerJumping);
            // if(cactus1.pos.x <= -12) cactus1.pos.x = widonwWidth;
            if(!isCactusOnScreen(cactus1.pos.x)) cactus1.pos.x = widonwWidth;
            if(!isCactusOnScreen(cactus4.pos.x)) cactus4.pos.x = widonwWidth;
            BeginDrawing();
            ClearBackground(RAYWHITE);
            DrawRectangleRec(cactus1.rec, cactus1.clr);
            DrawRectangleRec(cactus2.rec, cactus2.clr);
            DrawRectangleRec(cactus3.rec, cactus3.clr);
            DrawRectangleRec(cactus4.rec, cactus4.clr);
            DrawRectangleRec(cactus5.rec, cactus5.clr);
            DrawRectangleRec(cactus6.rec, cactus6.clr);
            DrawRectangleRec(ply.rec, ply.clr);
            DrawRectangleRec(ground.rec, ground.clr);
            printf("rand:%i\n",randomNumber());
            EndDrawing();
        }
        else{
            BeginDrawing();
            ClearBackground(RAYWHITE);
            DrawText("Press enter to retry", widonwWidth/2 - widonwWidth/4, widonwHeight/2, 50, BLACK);
            if(IsKeyPressed(KEY_ENTER)){
                cactus1.pos.x = cactus2.pos.x = cactus3.pos.x = offScreen+catusType1x;
                cactus1.pos.y = cactus2.pos.y = cactus3.pos.y = 422-(cactus1.size.y-1);
                cactus4.pos.x = cactus5.rec.x = cactus6.rec.x = (float)widonwWidth/2;
                cactus4.pos.y = cactus5.pos.y = cactus6.pos.y = 422-(cactus4.size.y-1);
                ply.pos.x = 56;
                ply.pos.y = 362;
                isPlayerDead = false;
            }
            EndDrawing();
        }
    }
    return 0;
}

bool isCactusOnScreen(int a){
    bool b = false;
    if(a >= 0 && a <= widonwWidth) b = true;
    else b = false;
    return b;
}

int randomNumber(){
    int a;
    srand(time(NULL));
    a = rand() % 6 + 1;
    return a;
}